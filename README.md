# CFPs.dev

## Prerequisites

1. [Netlify account](https://netlify.com)
1. [Fauna account](https://fauna.com/)
1. Install the [Netlify CLI](https://github.com/netlify/cli) with `npm install netlify-cli -g`
1. Log into the Netlify CLI with `netlify login`

## Development

### First time setup - NPM

- `npm install`

### First time setup - Netlify and Fauna

- Verify you are logged in with `netlify status`
- Initialize a new project with `netlify init`
- Select "Create and configure a new site"
- Select your team
- Choose a site name
- Select default build command, deploy folder and functions folder
- Choose to set up SSH key and webhook if desired
- Go to the Site in Netlify (through the "Admin URL")
- Click "Identity" and select "Enable Identity"
- Add and set up the [Fauna add on](https://docs.fauna.com/fauna/current/integrations/netlify) with
  - `netlify addons:create fauna`
  - `netlify addons:auth fauna`
- Give your database a name
- Deploy the site once with `npm run deploy` to enable all of the Netlify features you'll need in Development.

### First time setup - login

- If you have not yet set up the development connection for Netlify identity, put the URL of your site in the settings page for login (Find "Site URL" with the `netlify status` command)
- Sign up for a new account
- Confirm your email address.

## First time setup - database

- Go to the Fauna Dashboard and go to "Security"
- Add a new key with "Server" permissions
- Copy `.envDBseed.example` to `.envDBseed` in the project folder
- Replace the example Fauna secret with the secret key you just created
- Run `node faunaSeed.js` from the project folder.

`NOTE: This *should* be safe to re-run. You'll get errors for items that already exisit, so we don't today account for migrations. So when it doubt, start from scratch. And maybe don't use it in production 😉`

## Run development environment

- `netlify dev`

## Database

See [DATABASE.md](DATABASE.md)

## Tooling

- [Svelte Navigator](https://github.com/mefechoel/svelte-navigator)
- [Svelte Calendar](https://6edesign.github.io/svelte-calendar/)
- [Svelete Datepicker](https://github.com/beyonk-adventures/svelte-datepicker)
- [Day.js](https://day.js.org/)
