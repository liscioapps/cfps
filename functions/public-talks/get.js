const process = require('process');

const { Index, Match, Paginate, Get, Client, Ref, Collection, Call, Function } = require('faunadb')

const client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

const public = async (id) => {
    try {
        const userId = await client.query(Call(Function('userId_from_shortCode'), id))
        const response = await client.query(Paginate(Match(Index('published_talks_by_userId'), [true, userId])))
        const itemRefs = response.data
        const getAllItemsDataQuery = itemRefs.map((ref) => Get(ref))
        const ret = await client.query(getAllItemsDataQuery);
        const body = JSON.stringify(ret)
        return {
            statusCode: 200,
            body
        }
    } catch (error) {
        console.log('error', error)
        return {
            statusCode: 400,
            body: JSON.stringify(error),
        }
    }
}

module.exports = { public }