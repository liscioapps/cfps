const process = require('process');

const { Index, Match, Paginate, Get, Client, Ref, Collection, Call, Function } = require('faunadb')

const client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

const all = async (userId) => {
    console.log('Read all Talks');
    try {
        const response = await client.query(Paginate(Match(Index('talks_by_userId'), userId)))
        const itemRefs = response.data
        const getAllItemsDataQuery = itemRefs.map((ref) => Get(ref))
        const ret = await client.query(getAllItemsDataQuery);
        const body = JSON.stringify(ret)
        return {
            statusCode: 200,
            body
        }
    } catch (error) {
        console.log('error', error)
        return {
            statusCode: 400,
            body: JSON.stringify(error),
        }
    }
}

const one = async (userId, ref) => {
    console.log(`User ${userId} get Talk ${ref}`);
    try {
        const response = await client.query(Get(Ref(Collection('talks'), ref)));
        const body = JSON.stringify(response.data)
        if (response.data.userId === userId || response.data.published === true) {
            return {
                statusCode: 200,
                body
            }
        } else {
            return {
                statusCode: 401,
                body: JSON.stringify({ message: `Talk ${ref} is not associated with user ${userId}` })
            }
        }
    } catch (error) {
        console.log('error', error)
        return {
            statusCode: 400,
            body: JSON.stringify(error),
        }
    }
}

/**
 * 
 * This function takes a shortCode for a user, looks up their userId using the function userId_from_shortCode
 * With that userId, it use the published_talks_by_userId index to find all the public talks that user has published
 * 
 * @param {string} id The shortCode for the user (e.g. from cfps.dev/u/brendan)
 * @returns 
 */
const public = async (id) => {
    console.log(`Getting public talks for ${id}`)
    try {
        const userId = await client.query(Call(Function('userId_from_shortCode'), id))
        console.log(`shortCode ${id} yeilds userId ${userId}, getting public talks`)
        const response = await client.query(Paginate(Match(Index('published_talks_by_userId'), [true, userId])))
        const itemRefs = response.data
        const getAllItemsDataQuery = itemRefs.map((ref) => Get(ref))
        const ret = await client.query(getAllItemsDataQuery);
        const body = JSON.stringify(ret)
        return {
            statusCode: 200,
            body
        }
    } catch (error) {
        console.log('There was an error getting the public talks')
        console.log('error', error)
        if (error.requestResult.responseRaw.indexOf('value not found') > -1) {
            return {
                statusCode: 400,
                body: JSON.stringify({ message: `That user was not found.  Please try again.` }),
            }
        } else {
            return {
                statusCode: 400,
                body: JSON.stringify({ message: `There was an error getting the public talks ${error}` }),
            }
        }
    }
}


module.exports = { all, one, public }