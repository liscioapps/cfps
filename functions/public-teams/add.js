const process = require("process");

const { Get, Client, Ref, Select, Update, Match, Index } = require("faunadb");

const client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
});

const acceptInvite = async (userId, ref) => {
    console.log(`User ${userId} accept team invite ${ref}`);

    try {
        const team = await client.query(Get(Match(Index("teams_by_inviteLink"), ref)));

        if (!team.data.members.includes(userId)) {
            team.data.members.push(userId);
            const response = await client.query(Update(Select("ref", Get(Match(Index('teams_by_inviteLink'), ref))), { data: { ...team.data } }));
            console.log("success", response);
            return {
                statusCode: 200,
                body: JSON.stringify(response),
            };
        }

        return {
            statusCode: 418,
            body: JSON.stringify(team),
        };
    } catch (error) {
        console.log("error", error);
        return {
            statusCode: 400,
            body: JSON.stringify(error),
        };
    }
}

module.exports = { acceptInvite }