const process = require('process');

const { Index, Match, Paginate, Get, Client, Ref, Collection, Call, Function } = require('faunadb')

const client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

const public = async (id) => {
    try {
        const team = await client.query(Get(Match(Index("teams_by_inviteLink"), id)));
        const body = JSON.stringify({
            ref: team.ref,
            teamname: team.data.teamname,
        });
        return {
            statusCode: 200,
            body
        }
    } catch (error) {
        console.log('error', error)
        return {
            statusCode: 400,
            body: JSON.stringify(error),
        }
    }
}

module.exports = { public }