const process = require("process");

const { Get, Client, Ref, Collection, Update } = require("faunadb");

const client = new Client({
  secret: process.env.FAUNADB_SERVER_SECRET,
});

const one = async (userId, ref, event) => {
  const data = JSON.parse(event.body);
  console.log(`User ${userId} update Team ${ref}`);

  try {
    const thisTeam = await client.query(Get(Ref(Collection("teams"), ref)));

    if (thisTeam.data.owners.includes(userId)) {
      const response = await client.query(
        Update(Ref(Collection("teams"), ref), { data })
      );
      console.log("success", response);
      return {
        statusCode: 200,
        body: JSON.stringify(response),
      };
    } else {
      return {
        statusCode: 401,
        body: JSON.stringify({
          message: JSON.stringify({ message: `Team ${ref} is not associated with user ${userId}` })
        }),
      };
    }
  } catch (error) {
    console.log("error", error);
    return {
      statusCode: 400,
      body: JSON.stringify(error),
    };
  }
};

module.exports = { one };
