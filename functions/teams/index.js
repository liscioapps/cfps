const getTeams = require("./get");
const create = require("./create");
const updateTeams = require("./update");
const deleteCFPs = require("./delete");

const handler = async (event, context) => {
  const path = event.path.replace(/\.netlify\/functions\/[^/]+/, "");
  const segments = path.split("/").filter(Boolean);

  try {
    const { user, identity } = context.clientContext;
    let userId = "";
    if (user) {
      if (user.sub) {
        userId = user.sub;
      }
    }
    switch (event.httpMethod) {
      case "GET":
        if (segments.length === 0) return getTeams.all(userId);
        if (segments.length === 1) {
          const [id] = segments;
          return getTeams.one(userId, id);
        }
        if (segments.length === 2) {
          const [id, verb] = segments;
          if (verb === "cfps") {
            return getTeams.cfps(userId, id);
          } else {
            return {
              statusCode: 400,
              body: JSON.stringify({ message: "Unrecognized verb "})
            }
          }
        }
        return {
          statusCode: 400,
          body: JSON.stringify({ message: "Too many segments in GET request.  Either / ; /:id ; or /:id/cfps required" })
        };
      case "POST":
        return create.newTeam(event, userId);
      case "PUT":
        if (segments.length === 1) {
          const [id] = segments;
          return updateTeams.one(userId, id, event);
        }
        return {
          statusCode: 400,
          body: JSON.stringify({ message: "Invalid segments.  Must be of the form /:id" })
        };
      case "DELETE":
        //TODO
        if (segments.length === 1) {
          const [id] = segments;
          return deleteCFPs.one(userId, id);
        }
        return {
          statusCode: 400,
          body: JSON.stringify({ message: "Invalid segments.  Must be of the form /:id" })
        };
      default:
        console.log("Where is my large automobile?");
        break;
    }
  } catch (error) {
    console.log("error", error);
    return {
      statusCode: 500,
      body: JSON.stringify(error),
    };
  }
};

module.exports = { handler };
