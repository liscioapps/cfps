const sendMail = require('./sendmail');

const handler = async (event, context) => {
    console.log("Mail Postmark API");

    try {
        const path = event.path.replace(/\.netlify\/functions\/[^/]+/, '')
        const segments = path.split('/').filter(Boolean);
        console.log(context);
        const { user, identity } = context.clientContext;
        let userId = ''
        if (user) {
            if (user.sub) {
                userId = user.sub
            }
        }

        switch (event.httpMethod) {
            case 'GET':
                if (segments.length === 2) {
                    try {
                        console.log('Two segments', segments);
                        const [id, dir] = segments;
                        switch (dir) {
                            case 'welcome':
                                console.log(`Sending welcome ${id}`)
                                sendMail.welcome(id);
                                break;
                            case 'invite':
                                console.log(`Sending invite ${id}`)
                                //return getCFPs.public(id);
                            default:
                                return {
                                    statusCode: 404,
                                    body: JSON.stringify({ message: 'Not found dir'})
                                }
                        }
                        return {
                            statusCode: 200,
                            body: JSON.stringify({ message: "Done"})
                        }
                    } catch (error) {
                        console.log(error);
                        return {
                            statusCode: 400,
                            body: JSON.stringify(error)
                        }
                    }
                }
                return {
                    statusCode: 400,
                    body: JSON.stringify({ message: 'Incorrect segments in GET request.  Form of /type/:id required'})
                }
            case 'POST':

            default:
                console.log('Where is my large automobile?');
                break;
        }

    } catch (error) {
        console.log('General error in function');
        console.log('error', error)
        return {
            statusCode: 500,
            body: `There was an error ${error}`,
        }
    }
}

module.exports = { handler }