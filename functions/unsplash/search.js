const process = require("process");
const axios = require("axios");

const clientKey = process.env.UNSPLASH_KEY;
const baseURL = "https://api.unsplash.com";

const byString = async (event, userId) => {
  let url = new URL(`${baseURL}/search/photos/`);
  const str = event.queryStringParameters.query;
  if (str) {
    const params = {
      query: str,
      content_filter: "high",
      orientation: "landscape",
      client_id: clientKey,
    };
    Object.keys(params).forEach((key) =>
      url.searchParams.append(key, params[key])
    );

    const thisURL = url.toString();

    const res = await axios.get(thisURL);
    return {
      statusCode: 200,
      body: JSON.stringify(res.data),
    };
  } else {
    return {
      statusCode: 400,
      body: JSON.stringify({ message: "No query param" })
    };
  }
};

const download = async (event, userId, picId) => {
  let url = new URL(`${baseURL}/photos/${picId}/download`);
  const params = {
    client_id: clientKey,
  };
  Object.keys(params).forEach((key) =>
    url.searchParams.append(key, params[key])
  );

  const thisURL = url.toString();

  const res = await axios.get(thisURL);
  return {
    statusCode: 200,
    body: JSON.stringify(res.data),
  };
};

module.exports = { byString, download };
