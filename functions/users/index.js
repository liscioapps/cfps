const getProfile = require('./get');
const createUserProfile = require('./create');
const updateProfile = require('./update')
// const deleteTalks = require('./delete')

const handler = async (event, context) => {
    const path = event.path.replace(/\.netlify\/functions\/[^/]+/, '')
    const segments = path.split('/').filter(Boolean)

    try {
        const { user, identity } = context.clientContext;
        const userId = user.sub;
        switch (event.httpMethod) {
            case 'GET':
                if (segments.length === 0) return getProfile.one(userId);
                if (segments.length === 1) {
                    const [id] = segments;
                    return getProfile.isInUse(id);
                }
                return {
                    statusCode: 400,
                    body: JSON.stringify({ message: 'Too many segments in GET request.' }) //  Either / or /:id required'
                }
            case 'POST':
                return createUserProfile.setShortCode(event, userId);
            case 'PUT':
                if (segments.length === 1) {
                    const [id] = segments;
                    return updateProfile.one(userId, event);
                }
                if (segments.length === 2) {
                    const [id, dir] = segments;
                    switch (dir) {
                        case 'login':
                            console.log(`Record login for ${id}`)
                            return updateProfile.login(id, event);
                        default:
                            return {
                                statusCode: 404,
                                body: JSON.stringify({ message: 'Not found dir' })
                            }
                    }
                }
                return {
                    statusCode: 400,
                    body: JSON.stringify({ message: 'Invalid segments.  Must be of the form /:id or /:id/[action]' })
                }
            default:
                console.log('Where is my large automobile?')
                return {
                    statusCode: 400,
                    body: 'Where is my large automobile?'
                }
        }
    } catch (error) {
        console.log('error', error)
        return {
            statusCode: 500,
            body: JSON.stringify(error),
        }
    }
}

module.exports = { handler }