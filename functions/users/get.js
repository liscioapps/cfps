const process = require('process');

const { Index, Match, Paginate, Get, Client, Ref, Collection, Select, Call, Function } = require('faunadb')

const client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

const one = async (userId) => {
    console.log(`User ${userId} get profile`);
    try {
        const response = await client.query(
            Get(
                Select(["ref"], Get(Match(Index("user_profiles_userid"), userId)))
            )
        );
        const body = JSON.stringify(response.data)
        if (response.data.userId === userId) {
            return {
                statusCode: 200,
                body
            }
        } else {
            return {
                statusCode: 401,
                body: JSON.stringify({ message: `No user_profile record is associated with user ${userId}`})
            }
        }
    } catch (error) {
        console.log('error', error)
        return {
            statusCode: 400,
            body: JSON.stringify(error),
        }
    }
}

const isInUse = async (shortCode) => {
    console.log(`Check shortCode ${shortCode}`);
    try {
        const response = await client.query(Call(Function("shortCodeExists"), shortCode));
        console.log(response);
        if (response) {
            return {
                statusCode: 418,
                body: JSON.stringify({ mesage: `That code is in use` })
            }
        } else {
            return {
                statusCode: 200,
                body: JSON.stringify({ mesage: `Go for it, that code is free to use` })
            }
        }
    } catch (error) {
        console.log('error', error)
        return {
            statusCode: 400,
            body: JSON.stringify(error),
        }
    }
}

module.exports = { one, isInUse }