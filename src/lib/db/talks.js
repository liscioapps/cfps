import dayjs from "dayjs";
//import relativeTime from 'dayjs/plugin/relativeTime'
//dayjs.extend(relativeTime);

const url = "/.netlify/functions/talks"

function headers(user) {
    return {
        Authorization: `Bearer ${user.token.access_token}`
    }
}

export async function add(user, data) {
    return fetch(url, {
        method: "POST",
        body: JSON.stringify(data),
        headers: headers(user)
    })
}

export async function getAll(user) {
    return fetch(url, {
        method: "GET",
        headers: headers(user),
    }).then(response => { return response.json() })
}

export async function getOne(user, ref) {
    let opts = { method: "GET" }
    if (user) opts.headers = headers(user);

    return fetch(`${url}/${ref}`, opts).then(response => { return response.json() })
}

export async function update(user, ref, data) {
    return fetch(`${url}/${ref}`, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: headers(user)
    })
}

export async function deleteOne(user, ref) {
    return fetch(`${url}/${ref}`, {
        method: 'DELETE',
        headers: headers(user)
    })
}

export async function getPublic(shortCode) {
    return fetch(`${url}/public/${shortCode}`, {
        method: "GET",
    }).then(response => { return response.json() })
}

export function sortList(myCFPs, field) {
    return myCFPs.sort((a, b) => (a.data[field] > b.data[field] ? 1 : -1));
}