const url = "/.netlify/functions/users"

import { headers } from './utils'

/**
 * 
 * @param {Object} user - The current user (from stores / $currentUser)
 * @param {Object} data 
 * @param {string} data.shortCode - The short code to add to the user
 * @returns 
 */
export async function addShortCode(user, data) {
    return fetch(url, {
        method: "POST",
        body: JSON.stringify(data),
        headers: headers(user)
    })
}

export async function getMyProfile(user) {
    return fetch(url, {
        method: "GET",
        headers: headers(user)
    }).then(resp => resp.json())
}

export async function update(user, data) {
    return fetch(`${url}/${user.id}`, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: headers(user)
    })
}

export async function checkIfInUse(user, shortCode) {
    return fetch(`${url}/${shortCode}`, {
        method: "GET",
        headers: headers(user)
    })
}

export async function getShortCode(user) {
    const profile = await getMyProfile(user);
    return profile.shortCode;
}

export async function recordLogin(user) {
    return fetch(`${url}/${user.id}/login`, {
        method: 'PUT',
        body: JSON.stringify(user),
        headers: headers(user)
    })
}