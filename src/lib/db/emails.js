const url = "/.netlify/functions/mail"

import { headers } from './utils'

export async function sendEmail(user, type) {
    return fetch(`${url}/${user.id}/${type}`, {
        method: 'GET',
        headers: headers(user)
    })
}