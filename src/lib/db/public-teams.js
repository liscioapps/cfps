import { headers } from "./utils";

const url = "/.netlify/functions/public-teams";

export async function getOne(ref) {
  return fetch(`${url}/${ref}`, {
    method: "GET",
  }).then((response) => {
    return response.json();
  });
}

export async function accept(user, ref) {
  return fetch(`${url}/${ref}`, {
    method: 'PUT',
    headers: headers(user)
  }).then(response => { return response.json() })
}