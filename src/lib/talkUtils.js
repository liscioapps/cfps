import sanitizerPkg from "sanitizer";

export function getTalkURL(shortCode, thisTalk) {
    let talkURL = `/u/${shortCode}/${thisTalk.ref["@ref"].id}`;
    // #36: Support public slugs
    // if (thisTalk.data.slug) talkURL = `/u/${shortCode}/${thisTalk.data.slug}`;
    return talkURL;
}

export function getTalkAbstractSafe(thisTalk) {
    let talkAbstract = thisTalk.data?.abstract?.replaceAll("\n", "<br />");
    talkAbstract = sanitizerPkg.sanitize(talkAbstract);
    return talkAbstract;
}

export function getShortTalkAbstractSafe(thisTalk) {
    let shortAbstract = thisTalk.data?.abstract?.substring(0, 360) + "...";
    shortAbstract = sanitizerPkg.sanitize(shortAbstract);
    return shortAbstract;
}