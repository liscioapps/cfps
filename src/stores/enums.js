export const attendanceOpts = [
    {
        value: "Maybe",
        icon: "🤷",
        text: "Maybe"
    },
    {
        value: "Planned",
        icon: "📘",
        text: "Planned"
    },
    {
        value: "Speaking",
        icon: "🗣️",
        text: "Speaking"
    },
    {
        value: "Attendee",
        icon: "👀",
        text: "Attendee"
    },
    {
        value: "Notattending",
        icon: "🚷",
        text: "Not Attending"
    },
]

export function attendanceLookup(value) {
    return attendanceOpts.find((a) => a.value === value);
}

export function cfpStatusLookup(value) {
    return cfpStatusOpts.find((a) => a.value === value);
}

export const cfpStatusOpts = [
    {
        value: "Notplanned",
        icon: "👀",
        text: "Not Submitting"
    },
    {
        value: "Planned",
        icon: "🗣️",
        text: "Planned"
    },
    {
        value: "Submitted",
        icon: "🤞",
        text: "Submitted"
    },
    {
        value: "Accepted",
        icon: "✅",
        text: "Accepted"
    },
    {
        value: "Rejected",
        icon: "😢",
        text: "Rejected"
    },
]

export const talkStatusOpts = [
    {
        value: "Draft",
        icon: "🗣️",
        text: "Draft"
    },
    {
        value: "Submitted",
        icon: "🤞",
        text: "Submitted"
    },
    {
        value: "Writing",
        icon: "✍️",
        text: "Writing"
    },
    {
        value: "Written",
        icon: "✅",
        text: "Written"
    },
    {
        value: "Given",
        icon: "📣",
        text: "Given"
    },
]

export function talkStatusLookup(value) {
    return talkStatusOpts.find((a) => a.value === value);
}
